<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*Esta funcion determina donde se encuentra el usuario y le muestra un indicador en la pagina*/
function ruta_activa($cadena){
	$url=current_url(); 
		$separada = explode("/", $url);
		if (is_null($separada[6])) {
			redirect('shopping/index');
		}elseif ($separada[6]==$cadena) {
			echo "active";
		}
}

/*Esta funcion verifica si esta posicionado sobre la ruta de /shopping/loguin y 
	desabilita el link ademas determina si existe una sesión de usuario y activa el boton cerrar sesión*/
function hide_loguin(){
	$ci = & get_instance();
	$ci->load->library('session');
	
	$url=current_url(); 
	$array = explode("/", $url);

	if (isset($_SESSION['nombre'])) {
		echo "
			<div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
				<a class='dropdown-item' href='".base_url('shopping/cerrarsesion')." '>Cerrar sesión</a>
			</div>
			";
	}
	if ($array[6] == "loguin") {
		if (isset($_SESSION['nombre'])) {
			echo "
				<div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
					<a class='dropdown-item' href='".base_url('shopping/cerrarsesion')." '>Cerrar sesión</a>
				</div>
				";
		}else {
			echo "
			<div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
				<a class='dropdown-item' href='#'>desabilitado</a>
			</div>
		";
		}
	}else {
		echo "
			<div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
				<a class='dropdown-item' href='".base_url('shopping/loguin')." '>Iniciar sesión</a>
			</div>
			";
	}
}

//no sirve
function redireccion(string $url){
	header('location:'.$url.'');
}

//Funcion que determina la ubicación del usuario e imprime la ruta de donde se encuentra
function breadcrumb(){
	$url=current_url();
		$separada = explode("/", $url);
		if ($separada[6] == 'index' ) {

			echo " 
					<li class='breadcrumb-item active' aria-current='page'>Inicio</li>
				";
		}elseif ($separada[6] == 'productos' ) {
			echo " 
					<li class='breadcrumb-item'> <a href='".base_url()."shopping/index'>Inicio</a> </li>
					<li class='breadcrumb-item active' aria-current='page'>productos</li>
				";
		}elseif ($separada[6] == 'nosotros' ) {
			echo " 
					<li class='breadcrumb-item'> <a href='".base_url()."shopping/index'>Inicio</a> </li>
					<li class='breadcrumb-item active' aria-current='page'>nosotros</li>
				";
		}elseif ($separada[6] == 'contacto' ) {
			echo " 
					<li class='breadcrumb-item'> <a href='".base_url()."shopping/index'>Inicio</a> </li>
					<li class='breadcrumb-item active' aria-current='page'>productos</li>
				";
		}elseif ($separada[6] == 'loguin' ) {
			echo " 
					<li class='breadcrumb-item'> <a href='".base_url()."shopping/index'>Inicio</a> </li>
					<li class='breadcrumb-item active' aria-current='page'>loguin</li>
				";
		}
}

/*Funcion que valida la sesion de usuario y ademas si todo es correcto crea dos sesiones (flashData y sesion) 
	todos los datos los extrae de la base de datos y del formulario de iniciar sesión*/
	//La sesión que se crea es la que contiene el nombre de usuario para poder utilizarlo en diferentes operaciones
function validarLoguin($datos){
	if ($datos['loguin'] == null && $datos['correo'] == null && $datos['contrasena'] == null	) {
	//No hace nada en caso de ser false
	}else {
	//En caso de true realiza acción
		$ci= & get_instance();
		$ci->load->database();
		$ci->load->library('session');

		$ci->db->select('IdTipoUsuario as itu, Correo, Contrasena, Nombre');
		$ci->db->from('Usuario');

		$array = array(
			'Correo'=>$datos['correo'],
			'Contrasena'=>$datos['contrasena']
		);
		$ci->db->where($array);
		$consulta = $ci->db->get();
		$resultado = $consulta->row_array();

		$sesion = array(
			'nombre'=>$resultado['Nombre'],
			'nivel'=>$resultado['itu'],
			'logeo'=>FALSE
		);

		//echo json_encode($resultado['itu']);
		if (!is_null($resultado)) {
			if ($resultado['itu']==1) {
				//Se carga la sesión solo si el usuario tiene un nivel de Asociado
				$sesion['logeo']=TRUE;
				$ci->session->set_userdata($sesion);	
				$ci->session->set_flashdata('permiso',1);
				redirect('shopping/productos');

			}elseif ($resultado['itu']==3) {
				
				//Se carga la sesión solo si el usuario tiene un nivel de Asociado
				$sesion['logeo']=TRUE;
				$ci->session->set_userdata($sesion);	
				$ci->session->set_flashdata('permiso',3);
				
				redirect('shopping/administrator');
				

			}elseif ($resultado['itu']==6) {
					//Se carga la sesión solo si el usuario tiene un nivel de administrador
					$sesion['logeo']=TRUE;
					$ci->session->set_userdata($sesion);
					$ci->session->set_flashdata('permiso',6);

				redirect('shopping/administrator');
				
			}elseif($resultado['itu'] <= 0) {
				//echo json_encode($resultado);
				redirect('shopping/index');
				$ci->session->set_flashdata('permiso',2);
			}
		}elseif (is_null($resultado)) {
			$ci->session->set_flashdata('permiso',2);
			//echo "<br> is_null($ resultado) ";
		}
	}

	
}

//esta función extrae los datos de una sesion de tipo flashdata para poder imprimir un mensaje de bienvenida según sea el del nivel de acceso. 
function getFlashData(){
	$ci = & get_instance();
	$ci->load->library('session');
	$flash = $ci->session->flashdata('permiso');

	if (!is_null($flash)) {

		if($flash==1) {
			//echo "<script>console.log('1')</script>";
			echo " 
					<div class='alert alert-success alert-dismissible fade show' role='alert'>
						<strong>Bienvenido!</strong>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
					</div>
				";
		}elseif ($flash==2){
			//echo "<script>console.log('2')</script>";
			echo " 
					<div class='alert alert-danger alert-dismissible fade show' role='alert'>
						<strong>Ha sucedido un error intentalo mas tarde! </strong>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
					</div>
				";
		}elseif($flash==3) {
			//echo "<script>console.log('3')</script>";
			echo " 
					<div class='alert alert-success alert-dismissible fade show' role='alert'>
						<strong>Bienvenido Asociado!</strong>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
					</div>
				";
		}elseif ($flash==6) {
			//echo "<script>console.log('6')</script>";
			echo " 
					<div class='alert alert-success alert-dismissible fade show' role='alert'>
						<strong>Bienvenido Administrador!</strong>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
					</div>
				";
		}
	}

}

//esta funcion sirve para redirigir el acceso a las paginas de administrador.
function proteger(){
	$ci = & get_instance();
	$ci->load->library('session');

	if ( $ci->session->userdata('nombre') ==null ) {
		redirect('shopping/index');
		//echo "
		//		<script> alert('Sesion Nula');</script>		
		//";
	}
}

//esta funcion sirve para imprimir el nombre en las sesiones de administrador
function getSesname(){
	$ci = & get_instance();
	$ci->load->library('session');

	if ($ci->session->userdata('nombre')) {
		echo	$ci->session->userdata('nombre');
	}
		
}