<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function ruta_activa($cadena){
	$url=current_url(); 
		$separada = explode("/", $url);
		if (is_null($separada[6])) {
			redirect('shopping/loguin');
		}elseif ($separada[6]==$cadena) {
			echo "active";
		}
}

function hide_loguin(){
	$url=current_url(); 
	$array = explode("/", $url);
	if ($array[6] == "loguin") {
		echo "
			<div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
				<a class='dropdown-item' href='#'>desabilitado</a>
			</div>
		";
	}else {
		echo "
			<div class='dropdown-menu' aria-labelledby='navbarDropdownMenuLink'>
				<a class='dropdown-item' href='".base_url('shopping/loguin')." '>Iniciar sesión</a>
			</div>
			";
	}
}

function redireccion(string $url){
	header('location:'.$url.'');
}

function breadcrumb(){
	$url=current_url();
		$separada = explode("/", $url);
		if ($separada[6] == 'index' ) {

			echo " 
					<li class='breadcrumb-item active' aria-current='page'>Inicio</li>
				";
		}elseif ($separada[6] == 'productos' ) {
			echo " 
					<li class='breadcrumb-item'> <a href='".base_url()."shopping/index'>Inicio</a> </li>
					<li class='breadcrumb-item active' aria-current='page'>productos</li>
				";
		}elseif ($separada[6] == 'nosotros' ) {
			echo " 
					<li class='breadcrumb-item'> <a href='".base_url()."shopping/index'>Inicio</a> </li>
					<li class='breadcrumb-item active' aria-current='page'>nosotros</li>
				";
		}elseif ($separada[6] == 'contacto' ) {
			echo " 
					<li class='breadcrumb-item'> <a href='".base_url()."shopping/index'>Inicio</a> </li>
					<li class='breadcrumb-item active' aria-current='page'>productos</li>
				";
		}elseif ($separada[6] == 'loguin' ) {
			echo " 
					<li class='breadcrumb-item'> <a href='".base_url()."shopping/index'>Inicio</a> </li>
					<li class='breadcrumb-item active' aria-current='page'>loguin</li>
				";
		}
}

function validarLoguin($datos){
	if ($datos['loguin'] == null && $datos['correo'] == null && $datos['contrasena'] == null	) {
	//No hace nada en caso de ser false
	}else {
	//En caso de true realiza acción
		$ci= & get_instance();
		$ci->load->database();
		$ci->load->library('session');

		$ci->db->select('IdTipoUsuario as itu, Correo, Contrasena');
		$ci->db->from('Usuario');
		$array = array(
			'Correo'=>$datos['correo'],
			'Contrasena'=>$datos['contrasena']
		);

		$ci->db->where($array);
		$consulta = $ci->db->get();
		$resultado = $consulta->row_array();

		//echo json_encode($resultado['itu']);
		if (!is_null($resultado)) {
			if ($resultado['itu']==1) {
				$ci->session->set_flashdata('permiso',1);
				redirect('shopping/loguin');

			}elseif ($resultado['itu']==3) {
				$ci->session->set_flashdata('permiso',3);
				redirect('shopping/administrator');

			}elseif ($resultado['itu']==6) {
				$ci->session->set_flashdata('permiso',6);
				redirect('shopping/administrator');

			}elseif($resultado['itu'] <= 0) {
				echo json_encode($resultado);
				redirect('shopping/index');
				$ci->session->set_flashdata('permiso',2);
			}
		}elseif (is_null($resultado)) {
			$ci->session->set_flashdata('permiso',2);
			//echo "<br> is_null($ resultado) ";
		}
	}

	
}

function getFlashData(){
	$ci = & get_instance();
	$ci->load->library('session');
	$flash = $ci->session->flashdata('permiso');

	if (!is_null($flash)) {

		if($flash==1) {
			//echo "<script>console.log('1')</script>";
			echo " 
					<div class='alert alert-success alert-dismissible fade show' role='alert'>
						<strong>Bienvenido!</strong>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
					</div>
				";
		}elseif ($flash==2){
			//echo "<script>console.log('2')</script>";
			echo " 
					<div class='alert alert-danger alert-dismissible fade show' role='alert'>
						<strong>Ha sucedido un error intentalo mas tarde! </strong>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
					</div>
				";
		}elseif($flash==3) {
			//echo "<script>console.log('3')</script>";
			echo " 
					<div class='alert alert-success alert-dismissible fade show' role='alert'>
						<strong>Bienvenido Asociado!</strong>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
					</div>
				";
		}elseif ($flash==6) {
			//echo "<script>console.log('6')</script>";
			echo " 
					<div class='alert alert-success alert-dismissible fade show' role='alert'>
						<strong>Bienvenido Administrador!</strong>
							<button type='button' class='close' data-dismiss='alert' aria-label='Close'>
								<span aria-hidden='true'>&times;</span>
							</button>
					</div>
				";
		}
	}

	

}

//Crear función para poder validar datos de usuario en modal de registro
//Crear sesiones para todas las paginas y proteger pagina de administrador
//