<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asociado_model extends CI_Model{

            function __construct(){
                    $this->load->database();
                    $this->load->library('form_validation');
                    $this->load->library('session');
    }

//Crear petición ajax para registrar Asociado sin refrescar la pagina
    public function prepararRegistro($datos){
        if ( $datos['Registro']='asociado') {
                if ($datos['password'] == $datos['conf_password']) {
                        //creacion de array con los datos del formulario para registro en la base de datos
                        $datosVal = array(
                                array( 'field' => 'nombre', 'label' => 'Nombre de Usuario',    
                                'rules' => 'trim|required|min_length[3]|alpha_numeric_spaces' ),
                                
                                array( 'field' => 'correo', 'label' => 'Correo',               
                                'rules' => 'trim|required|valid_email'),
                        
                                array( 'field' => 'telefono', 'label' => 'Teléfono',             
                                'rules' => 'trim|required'),
                        
                                array( 'field' => 'direccion', 'label' => 'Dirección', 
                                'rules' => 'trim|required|min_length[10]'),
                                
                                array( 'field' => 'nomNegocio', 'label' => 'Nombre del Negocio', 
                                'rules' => 'trim|required|min_length[3]'),
                        
                                array( 'field' => 'password', 'label' => 'Contraseña',           
                                'rules' => 'trim|required|min_length[3]'),
                        
                                array( 'field' => 'fecNac', 'label' => 'Fecha de Nacimiento',  
                                'rules' => 'trim|required')
                        );
                                $this->form_validation->set_rules($datosVal);

                                //Se se validaron los campos correctamente
                                if ($this->form_validation->run() == FALSE ) {
                                       //Regresa un mensaje de error en caso de que los datos no esten bien validados
                                      echo "<script>
                                        Swal.fire(
                                                'Error!',
                                                '".json_encode(validation_errors())."',
                                                'error'
                                        )
                                        </script>";
                                }else {
                                        //se crea un array para poder controlar el ingreso de los datos a la base de datos
                                        $regDatos = array(
                                                'Nombre'          => $datos['nombre'],
                                                'IdTipoUsuario'   => 3,
                                                'Correo'          => $datos['correo'],
                                                'Telefono'        => $datos['telefono'],
                                                'Direccion'       => $datos['direccion'],
                                                'Contrasena'      => $datos['password'],
                                                'NombreNegocio'   => $datos['nomNegocio'],
                                                'FechaNacimiento' => $datos['Fecha_Nac']
                                        );
                                        $this->db->insert('Usuario',$regDatos);
                                        //Regresa un mensaje correcto
                                        echo "<script>
                                        Swal.fire({
                                                title: 'Registro Correcto',
                                                icon: 'success',
                                                html:
                                                'Verifica tu <b>Email</b> , ' +
                                                'para completar tu registro',
                                        })
                                        </script>";
                                }
                 }else {
                         //Regresa un mensaje de error indicando un error en las contraseñas
                        echo "<script>
                        Swal.fire(
                                'Error!',
                                'Las contraseñas no coinciden',
                                'error'
                        )
                        </script>";
                } 
        }
    }
}