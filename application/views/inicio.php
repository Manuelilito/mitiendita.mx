<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="archivos/css/bootstrap.min.css" rel="stylesheet">

    <!-- Personalizado CSS -->
    <link href="archivos/css/css_personalizado.css" rel="stylesheet">

    <title>Desarrollo web</title>
  </head>
  <body>
    <header>
      <div class="container-fluid"><!---   contenedor del Navbar   --->
        
	  <nav class="navbar navbar-expand-lg navbar-dark "  style="background-color: #B22222;">
				<a class=" card navbar-brand" href="#">
					<img src="uploads/img/logo_bien/logo_bien1.png" width="120" height="auto" class="d-inline-block align-top" alt="" loading="lazy">
				</a>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">

					<li class="nav-item active">
						<a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">Productos</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">Nosotros</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" href="#">Contacto</a>
					</li>

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Acceso
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="#">logueate</a>
						</div>
					</li>

				</ul>
				<li class="navbar-nav nav-item dropdown  px-4">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Registro
						</a>

						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" style="font-size: 14px;" href="#asociado" data-toggle="modal" data-target="#asociado">Asociado</a>
							<a class="dropdown-item" style="font-size: 14px;" href="#cliente"  data-toggle="modal" data-target="#cliente">Cliente</a>
						</div>
				</li>
			</div>

		</nav>	
              
                     <!--  <button type="button" class="btn btn-outline-light btn-lg px-2" style="font-size: 18px;" data-bs-toggle="modal" data-bs-target="#login">
                          &nbsp;&nbsp; Registro &nbsp;&nbsp;
                      </button> -->
              </div>
            </div>
          </div>
        </nav>
			<!-- Modal -->
					<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdrop" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="staticBackdrop">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Understood</button>
						</div>
						</div>
					</div>
					</div>

					<!--Modal de registro cliente-->
					<div class="modal fade" id="cliente" tabindex="-1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modal_cliente_staticBackdrop" aria-hidden="true" >
						<div class="modal-dialog">
						<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title display-4" id="cliente">Registro Cliente</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="GET" action="#hola.php" autocomplete="off" > <!---FORMULARIO-->
										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Nombre:</label>
											<input type="text" class="form-control" id="nombre" required="true">
										</div>
										
										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Correo:</label>
											<input type="email" class="form-control" id="correo" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Edad:</label>
											<input type="number" class="form-control" id="edad" required="true">
										</div>

										<div class="form-group">
											<label for="message-text" class="col-form-label">Contraseña:</label>
											<input type="password" class="form-control" id="password" required="true">
										</div>

										<div class="form-group">
											<label for="message-text" class="col-form-label">Confirma Contraseña:</label>
											<input type="password" class="form-control" id="conf_password" required="true">
										</div>

										<div class="modal-footer mt-4">
											<button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cerrar</button>
											<button type="submit" value="registro_cliente" class="btn btn-primary btn-lg ">Registro</button>
										</div>
									</form>   <!---FIN FORMULARIO-->
								</div>
							</div>
						</div>
					</div>
					<!--FIN DE MODAL-->

					<!--Modal de  Registro Cliente-->
					<div class="modal fade" id="asociado" tabindex="-1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modal_asociado_staticBackdrop" aria-hidden="true" >
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title display-4" id="asociado">Registro Asociado</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="GET" action="#hola.php" autocomplete="off" > <!---FORMULARIO-->

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Nombre:</label>
											<input type="text" class="form-control" id="nombre" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Nombre de Negocio:</label>
											<input type="text" class="form-control" id="negocio" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Correo:</label>
											<input type="text" class="form-control" id="correo" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Productos ofertados:</label>
											<input type="textarea" class="form-control" id="password" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">telefono:</label>
											<input type="text" class="form-control" id="telefono" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Ubicación:</label>
											<input type="text" class="form-control" id="ubicacion" required="true">
										</div>

										<div class="form-group">
											<label for="message-text" class="col-form-label">Contraseña:</label>
											<input type="password" class="form-control" id="password" required="true">
										</div>

										<div class="form-group">
											<label for="message-text" class="col-form-label">Confirma Contraseña:</label>
											<input type="password" class="form-control" id="conf_password" required="true">
										</div>

										<div class="modal-footer mt-4">
											<button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cerrar</button>
											<button type="submit" value="registro_asociado" class="btn btn-primary btn-lg ">Registro</button>
										</div>
									</form>   <!---FIN FORMULARIO-->
								</div>
							</div>
						</div>
					</div>
					<!--FIN DE MODAL-->
      </div>
    </header><!--FIN HEADER-->
      
    <div class="container mt-4 card card-body "><!---Contenedor para el contenido de la pagina--->
      
      
      <!-- SLIDER-->

    <div class="row mb-4">
		<div class="col-12">
			
		<div id="carousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carousel" data-slide-to="0" class="active"></li>
				<li data-target="#carousel" data-slide-to="1"></li>
				<li data-target="#carousel" data-slide-to="2"></li>
				<li data-target="#carousel" data-slide-to="3"></li>
				<li data-target="#carousel" data-slide-to="4"></li>
			</ol>

			<div class="carousel-inner">
				<div class="carousel-item active">
				<img src="uploads/img/banners/Banner_1_bien.jpg" class="d-block w-100" alt="...">
				<div class="carousel-caption d-none d-md-block" >
					<div class="text-secondary bg-dark text-light"> 
						<h5>Tu super a tu alcance</h5>
						<p>Con nuestras grandes ofertas quedaras impresionado</p>
					</div>
					
				</div>
				</div>

				<div class="carousel-item">
				<img src="uploads/img/banners/Banner_2_bien.jpg" class="d-block w-100" alt="...">
				<div class=" carousel-caption d-none d-md-block">
					<div class="text-secondary bg-dark text-light"> 	
						<h5>Online Shopping</h5>
						<p>Tu super a tu alcance</p>
					</div>
				</div>
				</div>

				<div class="carousel-item">
				<img src="uploads/img/banners/Banner_3_bien.jpg" class="d-block w-100" alt="...">
				<div class="carousel-caption d-none d-md-block">
					<div class="text-secondary bg-dark text-light"> 
						<h5>Third slide label</h5>
						<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
					</div>
				</div>
				</div>

				<div class="carousel-item">
				<img src="uploads/img/banners/Banner_4_bien.jpg" class="d-block w-100" alt="...">
				<div class="carousel-caption d-none d-md-block">
					<div class="text-secondary bg-dark text-light"> 
						<h5>Four slide label</h5>
						<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
					</div>
				</div>
				</div>

				<div class="carousel-item">
				<img src="uploads/img/banners/Banner_5_bien.jpg" class="d-block w-100" alt="...">
				<div class="carousel-caption d-none d-md-block">
					<div class="text-secondary bg-dark text-light">
						<h5>Five slide label</h5>
						<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
					</div>
				</div>
				</div>


			</div>

			<a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>

			<a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>

		</div>

        </div>
    </div> 


      
      <!--FIN DE  SLIDER -->

      <div class="row mt-4 mb-4 centcont">
        <div class="col-12">
          
          <div class="row mt-4"><!--Comienzan los card de los productos-->
            <div class="col-sm">
              <div class="card carsiz18 " >
                <img src="uploads/img/product_ejemplo.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
  
            <div class="col-sm">
              <div class="card carsiz18" >
                <img src="uploads/img/product_ejemplo.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
  
            <div class="col-sm ">
              <div class="card carsiz18" >
                <img src="uploads/img/product_ejemplo.jpeg" class="card-img-top " alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
          </div>
  
          <div class="row mt-4">
            <div class="col-sm ">
              <div class="card carsiz18">
                <img src="uploads/img/product_ejemplo.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
  
            </div>
            <div class="col-sm ">
              <div class="card carsiz18" >
                <img src="uploads/img/product_ejemplo.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
  
            <div class="col-sm ">
              <div class="card carsiz18" >
                <img src="uploads/img/product_ejemplo.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
          </div><!--Finalizan card de los productos-->  


        </div>     
      </div>
        
      
    </div> <!--Fin de contenido de página-->
 


    <!--Inicia el pie de página--->
      <footer class="bg-dark text-white text-center text-lg-start">
        <!-- Grid container -->
        <div class="container-fluid p-4">
          <!--Grid row-->
          <div class="row">
            <!--Grid column-->
            <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
              <h5 class="text-uppercase">Contenido del pie de página</h5>
      
              <p>
                Ofrecemos los mejores productos del mercado de los mejores proveedores, nosotros 
                te vamos a vender a un precio insuperable, nuestros asociados son personas confiables y sinceras.
                Que esperas a comprar con nosotros.
              </p>
            </div>
            <!--Grid column-->
      
            <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase">Proveedores</h5>
    
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#donpedrito" class="text-white">Don pedrito</a>
              </li>
              <li>
                <a href="#superamar" class="text-white">Superamar</a>
              </li>
              <li>
                <a href="#minisuper" class="text-white">Mini Super</a>
              </li>
              <li>
                <a href="#plastimundo" class="text-white">Plastimundo</a>
              </li>
            </ul>
          </div>
          <!--Grid column-->
    
          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase mb-0">Almacenes</h5>
    
            <ul class="list-unstyled">
              <li>
                <a href="#labodegota" class="text-white">La bodegota</a>
              </li>
              <li>
                <a href="#elalmacen" class="text-white">El Almacén</a>
              </li>
              <li>
                <a href="#almacenesdelcentro" class="text-white">Almacenes del centro</a>
              </li>
              <li>
                <a href="#piojo's" class="text-white">Piojo's</a>
              </li>
            </ul>
          </div>
          <!--Grid column-->
          </div>
          <!--Grid row-->
        </div>
        <!-- Grid container -->
      
        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
          © 2020 Copyright:
          <a class="text-white" href="#Desarrollo de aplicaciones web">Desarrollo de aplicaciones web</a>
        </div>
        <!-- Copyright -->
      </footer>
  <!---Termina el pie de página--->
    <script>


		$('#modal').modal()

		$('.carousel').carousel({
			intervar:10000,
            pause: hover

		})
			
		

    </script>

<script src="archivos/js/jquery-3.2.1.slim.min.js"></script>
<script src="archivos/js/bootstrap.min.js"></script>

</body>
</html>