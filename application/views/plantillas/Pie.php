    <!--Inicia el pie de página--->
      <footer class="bg-dark text-white text-center text-lg-start">
        <!-- Grid container -->
        <div class="container-fluid p-4">
          <!--Grid row-->
          <div class="row">
            <!--Grid column-->
            <div class="col-lg-6 col-md-12 mb-4 mb-md-0">
              <h5 class="text-uppercase">Contenido del pie de página</h5>
      
              <p>
                Ofrecemos los mejores productos del mercado de los mejores proveedores, nosotros 
                te vamos a vender a un precio insuperable, nuestros asociados son personas confiables y sinceras.
                Que esperas a comprar con nosotros.
              </p>
            </div>
            <!--Grid column-->
      
            <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase">Proveedores</h5>
    
            <ul class="list-unstyled mb-0">
              <li>
                <a href="#donpedrito" class="text-white">Don pedrito</a>
              </li>
              <li>
                <a href="#superamar" class="text-white">Superamar</a>
              </li>
              <li>
                <a href="#minisuper" class="text-white">Mini Super</a>
              </li>
              <li>
                <a href="#plastimundo" class="text-white">Plastimundo</a>
              </li>
            </ul>
          </div>
          <!--Grid column-->
    
          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase mb-0">Almacenes</h5>
    
            <ul class="list-unstyled">
              <li>
                <a href="#labodegota" class="text-white">La bodegota</a>
              </li>
              <li>
                <a href="#elalmacen" class="text-white">El Almacén</a>
              </li>
              <li>
                <a href="#almacenesdelcentro" class="text-white">Almacenes del centro</a>
              </li>
              <li>
                <a href="#piojo's" class="text-white">Piojo's</a>
              </li>
            </ul>
          </div>
          <!--Grid column-->
          </div>
          <!--Grid row-->
        </div>
        <!-- Grid container -->
      
        <!-- Copyright -->
        <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
          © 2020 Copyright:
          <a class="text-white" href="#Desarrollo de aplicaciones web">Desarrollo de aplicaciones web</a>
        </div>
        <!-- Copyright -->
      </footer>
  <!---Termina el pie de página--->

</body>
</html>