
<!---Contenedor para el contenido de la pagina--->
<div class="container mt-4 card card-body ">
      
      <!-- SLIDER-->

		<div class="row mb-4">
			<div class="col-12">
				
			<div id="carousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel" data-slide-to="0" class="active"></li>
					<li data-target="#carousel" data-slide-to="1"></li>
					<li data-target="#carousel" data-slide-to="2"></li>
					<li data-target="#carousel" data-slide-to="3"></li>
					<li data-target="#carousel" data-slide-to="4"></li>
				</ol>

				<div class="carousel-inner">
					<div class="carousel-item active">
					<img src="<?php echo base_url();?>uploads/img/banners/Banner_1_bien.jpg" class="d-block w-100" alt="...">
					<div class="carousel-caption d-none d-md-block" >
						<div class="text-secondary bg-dark text-light"> 
							<h5>Tu super a tu alcance</h5>
							<p>Con nuestras grandes ofertas quedaras impresionado</p>
						</div>
						
					</div>
					</div>

					<div class="carousel-item">
					<img src="<?php echo base_url();?>uploads/img/banners/Banner_2_bien.jpg" class="d-block w-100" alt="...">
					<div class=" carousel-caption d-none d-md-block">
						<div class="text-secondary bg-dark text-light"> 	
							<h5>Online Shopping</h5>
							<p>Tu super a tu alcance</p>
						</div>
					</div>
					</div>

					<div class="carousel-item">
					<img src="<?php echo base_url();?>uploads/img/banners/Banner_3_bien.jpg" class="d-block w-100" alt="...">
					<div class="carousel-caption d-none d-md-block">
						<div class="text-secondary bg-dark text-light"> 
							<h5>Third slide label</h5>
							<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
						</div>
					</div>
					</div>

					<div class="carousel-item">
					<img src="<?php echo base_url();?>uploads/img/banners/Banner_4_bien.jpg" class="d-block w-100" alt="...">
					<div class="carousel-caption d-none d-md-block">
						<div class="text-secondary bg-dark text-light"> 
							<h5>Four slide label</h5>
							<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
						</div>
					</div>
					</div>

					<div class="carousel-item">
					<img src="<?php echo base_url();?>uploads/img/banners/Banner_5_bien.jpg" class="d-block w-100" alt="...">
					<div class="carousel-caption d-none d-md-block">
						<div class="text-secondary bg-dark text-light">
							<h5>Five slide label</h5>
							<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
						</div>
					</div>
					</div>


				</div>

				<a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>

				<a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>

			</div>

			</div>
		</div> 


      
      <!--FIN DE  SLIDER -->

      <div class="row mt-4 mb-4 centcont">
        <div class="col-12">
          
          <div class="row mt-4"><!--Comienzan los card de los productos-->
            <div class="col-sm">
              <div class="card carsiz18 " >
                <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
  
            <div class="col-sm">
              <div class="card carsiz18" >
                <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
  
            <div class="col-sm ">
              <div class="card carsiz18" >
                <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top " alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
          </div>
  
          <div class="row mt-4">
            <div class="col-sm ">
              <div class="card carsiz18">
                <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>

            <div class="col-sm ">
              <div class="card carsiz18" >
                <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
  
            <div class="col-sm ">
              <div class="card carsiz18" >
                <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                <div class="card-body">
                  <h5 class="card-title">Titulo</h5>
                  <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                  <a href="#" class="btn btn-primary">Comprar</a>
                </div>
              </div>
            </div>
          </div><!--Finalizan card de los productos-->  
        </div>     
      </div>
    </div> <!--Fin de contenido de página-->
 