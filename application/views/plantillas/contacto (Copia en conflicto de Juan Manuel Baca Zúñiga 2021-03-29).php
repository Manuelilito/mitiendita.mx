<!---Contenido contenido de la pagina--->
    <div class="container mt-4 card card-body ">

      <div class="row">
        <div class="col" >
          <h1 class="d-grid gap-2 col-5 mx-auto display-3">Contactanos</h1> 
         <p>
           Todos los campos con <strong class="text-danger" >*</strong> con requeridos
         </p>
        </div>

      </div>

      <div class="row mt-4">
        <div class="col">

          <!---FORMULARIO-->
          <form method="GET" action="#hola.php" autocomplete="off" >

            <div class="mb-3">
              <label for="name" class="form-label">* Nombre </label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Tu nombre" required>
            </div>

            <div class="mb-3">
              <label for="email" class="form-label">* Correo Electronico</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="correo@servidor.com" required>
            </div>

            <div class="mb-3">
              <label for="telefono" class="form-label">* Teléfono</label>
              <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" required>
            </div>

            <div class="mb-3">
              <label for="mensaje" class="form-label">* Tu mensaje</label>
              <textarea class="form-control" id="mensaje" name="mensaje" rows="3" required ></textarea>
            </div>

            <div class="mb-3 pull-right mt-4 modal-footer">
              <div class="d-grid gap-2 col-12">
              <button type="submit" value="contacto" class="btn btn-outline-info btn-lg">Enviar</button>
              </div>   
            </div>

          </form>
          <!---FIN FORMULARIO-->

        </div>
        
      </div>

    </div>
    <!---Fin de contenido  de la pagina--->
 
