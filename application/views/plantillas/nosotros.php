<!---Contenido contenido de la pagina--->
    <div class="container mt-4 card card-body ">
      <div class="row mt-4 mb-4 ">
        <div class="col-12 text-center">
          <h1 class="display-1">Nosotros</h1>
        </div>

        <div class="row">
          <div class="col-12">
            <h4>¿Quienes Somos?</h4> 
            <p>
              JM "Tú super en linea" nace a traves de la necesidad de 
              apoyo al comercio local durante la contingencia sanitaria
              que estamos atravesando desde el año 2020, puesto que esta
              situación afecto sobre todo a comercios pequeños y locales 
              los cuales han perdido muchas ventas y productos en los que
              han invertido. El comercio online es la mejor opcion en ventas
              durante esta situación en donde se busca exponerse lo menos
              a salir de casa.</p>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <h4>Misión</h4> 
            <p>
              Nuestro compromiso es apoyar el comercio local a traves
              del servicio ofrecido con la venta de sus productos principales
              en linea apoyandolos su economia durante esta contingencia sanitaria
              y satisfaciendo las necesidades de la población en la adquisición 
              de productos de primera necesidad.</p>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <h4>Vision:</h4> 
            <p>
              Ser una de las plataformas principales en el mercado local
              para la venta y distribución de productos de primera 
              necesidad.
            </p>
          </div>
        </div>

      </div>
    </div>
    <!---Fin de contenido  de la pagina--->
 
 