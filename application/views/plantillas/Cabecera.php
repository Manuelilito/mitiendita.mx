<?php $this->load->helper('personalizados_helper');?> 
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>archivos/css/bootstrap.min.css" rel="stylesheet">

    <!-- Personalizado CSS -->
    <link href="<?php echo base_url();?>archivos/css/css_personalizado.css" rel="stylesheet">
	
	<!-- Archivos JS -->
	<script language="JavaScript" type="text/javascript" src="<?php echo base_url();?>archivos/js/jquery-3.2.1.slim.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?php echo base_url();?>archivos/js/bootstrap.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?php echo base_url();?>archivos/js/personalizado.js"></script>


    <title>Comercio electronico</title>
  </head>
  <body>
    <header>
      <div class="container-fluid"><!---   contenedor del Navbar   --->
        
	  <nav class="navbar navbar-expand-lg navbar-dark "  style="background-color: #B22222;">
				<a class=" card navbar-brand" href="#">
					<img src="<?php echo base_url();?>uploads/img/logo_bien/logo_bien1.png" width="120" height="auto" class="d-inline-block align-top" alt="" loading="lazy">
				</a>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item <?php ruta_activa("index"); ?>">
						<a class="nav-link" href="<?php echo base_url()?>shopping/index">Inicio <span class="sr-only">(current)</span></a>
					</li>

					<li class="nav-item <?php ruta_activa("productos"); ?>">
						<a class="nav-link" href="<?php echo base_url()?>shopping/productos">Productos</a>
					</li>

					<li class="nav-item <?php ruta_activa("nosotros");?>">
						<a class="nav-link" href="<?php echo base_url()?>shopping/nosotros" >Nosotros</a>
					</li>

					<li class="nav-item <?php ruta_activa("contacto");?> ">
						<a class="nav-link" href="<?php echo base_url()?>shopping/contacto">Contacto</a>
					</li>

					<li class="nav-item dropdown <?php ruta_activa("loguin");?> ">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Acceso
						</a>
						<!-- Ocultar iniciar sesión si se encuentra en ruta /shopping/loguin -->
						<?php hide_loguin();?>
					</li>

				</ul>
				<li class="navbar-nav nav-item dropdown  px-4">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Registro
						</a>

						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" style="font-size: 14px;" href="#asociado" data-toggle="modal" data-target="#asociado">Asociado</a>
							<a class="dropdown-item" style="font-size: 14px;" href="#cliente"  data-toggle="modal" data-target="#cliente">Cliente</a>
						</div>
				</li>

				<li class="navbar-nav nav-item  px-4">
					<a class="nav-link" href="#">
						<img src="<?php echo base_url()?>archivos/css/icons/cart4.svg" width="25" height="25" alt="Bootstrap" >2
					</a>
				</li>
			</div>

		</nav>	
              
                     <!--  <button type="button" class="btn btn-outline-light btn-lg px-2" style="font-size: 18px;" data-bs-toggle="modal" data-bs-target="#login">
                          &nbsp;&nbsp; Registro &nbsp;&nbsp;
                      </button> -->
              </div>
            </div>
          </div>
        </nav>
			<!-- Modal -->
					<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdrop" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="staticBackdrop">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Understood</button>
						</div>
						</div>
					</div>
					</div>

					<!--Modal de registro cliente-->
					<div class="modal fade" id="cliente" tabindex="-1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modal_cliente_staticBackdrop" aria-hidden="true" >
						<div class="modal-dialog">
						<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title display-4" id="cliente">Registro Cliente</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="GET" action="#hola.php" autocomplete="off" > <!---FORMULARIO-->
										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Nombre:</label>
											<input type="text" class="form-control" id="nombre" required="true">
										</div>
										
										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Correo:</label>
											<input type="email" class="form-control" id="correo" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Edad:</label>
											<input type="number" class="form-control" id="edad" required="true">
										</div>

										<div class="form-group">
											<label for="message-text" class="col-form-label">Contraseña:</label>
											<input type="password" class="form-control" id="password" required="true">
										</div>

										<div class="form-group">
											<label for="message-text" class="col-form-label">Confirma Contraseña:</label>
											<input type="password" class="form-control" id="conf_password" required="true">
										</div>

										<div class="modal-footer mt-4">
											<button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cerrar</button>
											<button type="submit" value="registro_cliente" class="btn btn-primary btn-lg ">Registro</button>
										</div>
									</form>   <!---FIN FORMULARIO-->
								</div>
							</div>
						</div>
					</div>
					<!--FIN DE MODAL-->

					<!--Modal de  Registro Cliente-->
					<div class="modal fade" id="asociado" tabindex="-1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modal_asociado_staticBackdrop" aria-hidden="true" >
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title display-4" id="asociado">Registro Asociado</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<form method="GET" action="#hola.php" autocomplete="off" > <!---FORMULARIO-->

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Nombre:</label>
											<input type="text" class="form-control" id="nombre" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Nombre de Negocio:</label>
											<input type="text" class="form-control" id="negocio" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Correo:</label>
											<input type="text" class="form-control" id="correo" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Productos ofertados:</label>
											<input type="textarea" class="form-control" id="password" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">telefono:</label>
											<input type="text" class="form-control" id="telefono" required="true">
										</div>

										<div class="form-group">
											<label for="recipient-name" class="col-form-label">Ubicación:</label>
											<input type="text" class="form-control" id="ubicacion" required="true">
										</div>

										<div class="form-group">
											<label for="message-text" class="col-form-label">Contraseña:</label>
											<input type="password" class="form-control" id="password" required="true">
										</div>

										<div class="form-group">
											<label for="message-text" class="col-form-label">Confirma Contraseña:</label>
											<input type="password" class="form-control" id="conf_password" required="true">
										</div>

										<div class="modal-footer mt-4">
											<button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cerrar</button>
											<button type="submit" value="registro_asociado" class="btn btn-primary btn-lg ">Registro</button>
										</div>
									</form>   <!---FIN FORMULARIO-->
								</div>
							</div>
						</div>
					</div>
					<!--FIN DE MODAL-->
      </div>
    </header><!--FIN HEADER-->

	<div class="container-fluid ">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
						<? breadcrumb(); ?>
				</ol>
			</nav>
	</div>
	