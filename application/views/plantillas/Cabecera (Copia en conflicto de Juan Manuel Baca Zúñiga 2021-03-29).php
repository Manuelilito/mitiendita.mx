<?php 
	$this->load->helper('personalizados_helper');
	$this->load->model('Cliente_model');
	$this->load->model('Asociado_model');
	$this->load->library('session');

	$ci = & get_instance();
	$regclient = $ci->session->flashdata('registroCliente');
	$fcontra = $ci->session->flashdata('contrasena');

?> 
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>archivos/css/bootstrap.min.css" rel="stylesheet">

    <!-- Personalizado CSS -->
    <link href="<?php echo base_url();?>archivos/css/css_personalizado.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.7/sweetalert2.css" rel="stylesheet">
	
	<!-- Archivos JS -->
	<script language="JavaScript" type="text/javascript" src="<?php echo base_url();?>archivos/js/jquery-3.2.1.slim.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?php echo base_url();?>archivos/js/bootstrap.min.js"></script>
	<script language="JavaScript" type="text/javascript" src="<?php echo base_url();?>archivos/js/personalizado.js"></script>
	<script language="JavaScript" type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.7/sweetalert2.min.js



"></script>
	
    <title>Comercio electronico</title>
  </head>
  <body>
    <header>
      <div class="container-fluid"><!---   contenedor del Navbar   --->
        
	  <nav class="navbar navbar-expand-lg navbar-dark "  style="background-color: #B22222;">
				<a class=" card navbar-brand" href="#">
					<img src="<?php echo base_url();?>uploads/img/logo_bien/logo_bien1.png" width="120" height="auto" class="d-inline-block align-top" alt="" loading="lazy">
				</a>

			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item <?php ruta_activa("index"); ?>">
						<a class="nav-link" href="<?php echo base_url()?>shopping/index">Inicio <span class="sr-only">(current)</span></a>
					</li>

					<li class="nav-item <?php ruta_activa("productos"); ?>">
						<a class="nav-link" href="<?php echo base_url()?>shopping/productos">Productos</a>
					</li>

					<li class="nav-item <?php ruta_activa("nosotros");?>">
						<a class="nav-link" href="<?php echo base_url()?>shopping/nosotros" >Nosotros</a>
					</li>

					<li class="nav-item <?php ruta_activa("contacto");?> ">
						<a class="nav-link" href="<?php echo base_url()?>shopping/contacto">Contacto</a>
					</li>

					<li class="nav-item dropdown <?php ruta_activa("loguin");?> ">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Acceso
						</a>

						<!-- Leer descripcion en area de funciones-->
						<?php hide_loguin();?>
					</li>

				</ul>
				<li class="navbar-nav nav-item dropdown  px-4">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Registro
						</a>

						<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" style="font-size: 14px;" href="#asociado" data-toggle="modal" data-target="#asociado">Asociado</a>
							<a class="dropdown-item" style="font-size: 14px;" href="#cliente"  data-toggle="modal" data-target="#cliente">Cliente</a>
						</div>
				</li>

				<li class="navbar-nav nav-item  px-4">
					<a class="nav-link" href="#">
						<img src="<?php echo base_url()?>archivos/css/icons/cart4.svg" width="25" height="25" alt="Bootstrap" >2
					</a>
				</li>
			</div>

		</nav>	
              
                     <!--  <button type="button" class="btn btn-outline-light btn-lg px-2" style="font-size: 18px;" data-bs-toggle="modal" data-bs-target="#login">
                          &nbsp;&nbsp; Registro &nbsp;&nbsp;
                      </button> -->
              </div>
            </div>
          </div>
        </nav>
			<!-- Modal -->
					<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdrop" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="staticBackdrop">Modal title</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							...
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="button" class="btn btn-primary">Understood</button>
						</div>
						</div>
					</div>
					</div>

					<!--Modal de registro cliente-->
					<div class="modal fade" id="cliente" tabindex="-1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modal_cliente_staticBackdrop" aria-hidden="true" >
						<div class="modal-dialog">
						<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title display-4" id="cliente">Registro Cliente</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								<!---<form method="GET" action="#hola.php" autocomplete="off" > FORMULARIO-->
								<?= form_open(); ?>
										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('nombre');?></label>
											<label for="nombre" class="col-form-label">Nombre:</label>
											<input type="text" class="form-control" id="nombre" name="nombre" required>
										</div>
										
										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('correo');?></label>
											<label for="recipient-name" class="col-form-label">Correo:</label>
											<input type="email" class="form-control" id="correo" name="correo" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('telefono');?></label>
											<label for="telefono" class="col-form-label">Teléfono:</label>
											<input type="tel" class="form-control" placeholder="xxx-xxx-xxxx" id="telefono" name="telefono" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" maxlength="12" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('fecNac');?></label>
											<label for="fecNac" class="col-form-label">Fecha de Nacimiento:</label>
											<input type="date" class="form-control" id="fecNac" name="fecNac" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('direccion');?></label>
											<label for="direccion" class="col-form-label">Dirección:</label>
											<input type="text" class="form-control" id="direccion" name="direccion" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('password');?></label>
											<label for="password" class="col-form-label">Contraseña:</label>
											<input type="password" class="form-control" id="password" name="password" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('conf_password');?></label>
											<label for="conf_password" class="col-form-label">Confirma Contraseña:</label>
											<input type="password" class="form-control" id="conf_password"  name="conf_password" required>
											<input type="hidden" id="Registro" name="Registro" value="cliente">
											<input type="hidden" id="url" name="url" value="<?php current_url(); ?>">
										</div>

										<div class="modal-footer mt-4">
											<button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cerrar</button>
											<button type="submit" value="registro_cliente" class="btn btn-primary btn-lg ">Registro</button>
											<input type="hidden" id="Registro" name="Registro" value="cliente">
										</div>
										<?php echo form_close(); ?>    <!---</form>   FIN FORMULARIO-->
								</div>
							</div>
						</div>
					</div>
					<!--FIN DE MODAL-->

					<!--Modal de  Registro Asociado-->
					<div class="modal fade" id="asociado" tabindex="-1" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="modal_asociado_staticBackdrop" aria-hidden="true" >
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title display-4" id="asociado">Registro Asociado</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
								
         						 <?= form_open(); ?>
									<!--<form method="GET" action="#hola.php" autocomplete="off" >--> <!---FORMULARIO-->

									<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('nombre');?></label>
											<label for="nombre" class="col-form-label">Nombre:</label>
											<input type="text" class="form-control" id="nombre" name="nombre" required>
										</div>
										
										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('correo');?></label>
											<label for="recipient-name" class="col-form-label">Correo:</label>
											<input type="email" class="form-control" id="correo" name="correo" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('telefono');?></label>
											<label for="telefono" class="col-form-label">Teléfono:</label>
											<input type="tel" class="form-control" placeholder="xxx-xxx-xxxx" id="telefono" name="telefono" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" maxlength="12" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('fecNac');?></label>
											<label for="fecNac" class="col-form-label">Fecha de Nacimiento:</label>
											<input type="date" class="form-control" id="fecNac" name="fecNac" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('direccion');?></label>
											<label for="direccion" class="col-form-label">Dirección:</label>
											<input type="text" class="form-control" id="direccion" name="direccion" required>
										</div>

										<div class="form-group">
											<label for="nomNegocio" class="col-form-label" ><?= form_error('direccion');?></label>
											<label for="nomNegocio" class="col-form-label">Nombre de negocio:</label>
											<input type="text" class="form-control" id="nomNegocio" name="nomNegocio" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('password');?></label>
											<label for="password" class="col-form-label">Contraseña:</label>
											<input type="password" class="form-control" id="password" name="password" required>
										</div>

										<div class="form-group">
											<label for="nombre" class="col-form-label" ><?= form_error('conf_password');?></label>
											<label for="conf_password" class="col-form-label">Confirma Contraseña:</label>
											<input type="password" class="form-control" id="conf_password"  name="conf_password" required>
										</div>

										<div class="modal-footer mt-4">
											<button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cerrar</button>
											<button type="submit" value="registro_cliente" class="btn btn-primary btn-lg ">Registro</button>
											<input type="hidden" id="Registro" name="Registro" value="asociado">
										</div>

									<?= form_close(); ?><!--  </form> -->   <!---FIN FORMULARIO-->

								</div>
							</div>
						</div>
					</div>
					<!--FIN DE MODAL-->
      </div>
    </header><!--FIN HEADER-->
 
	<?php
	$ci = & get_instance();
		$userData = array(
			'nombre'     	   => $ci->input->post('nombre'),
			'correo'   	       => $ci->input->post('correo'),
			'telefono'  	   => $ci->input->post('telefono'),
			'Fecha_Nac'  	   => $ci->input->post('fecNac'),
			'direccion' 	   => $ci->input->post('direccion'),
			'password'         => $ci->input->post('password'),
			'nomNegocio'       => $ci->input->post('nomNegocio'),
			'conf_password'    => $ci->input->post('conf_password')
		);
		if (!is_null($userData) ) {
//validacion de no nulo sobre campo hidden de cliente
			if ( !is_null($ci->input->post('Registro')) ) {
				if ($ci->input->post('Registro') == 'cliente') {
					$ci->Cliente_model->prepararRegistro($userData);

				}elseif ($ci->input->post('Registro') == 'asociado') {
					$ci->Asociado_model->prepararRegistro($userData);
				}
			}
		}
	?>
	<div class="container-fluid ">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
						<? breadcrumb(); ?>
				</ol>
			</nav>
	</div>
	