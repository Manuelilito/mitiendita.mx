<!---Contenido contenido de la pagina--->
    <div class="container mt-4 card card-body ">

      <div class="row">
        <div class="col" >
          <h1 class="d-grid gap-2 col-5 mx-auto display-3">Inicia Sesión</h1> 
         </div>

      </div>

      <div class="row mt-4">
        <div class="col">

          <!---Form de Usuario-->
          <?php $hidden = array('loguin'=>'valueformloguin');?>
          <?= form_open('shopping/loguin','' ,$hidden); ?>

            <div class="mb-3">
              <label for="correo" class="form-label">* Correo </label>
              <input type="email" class="form-control" id="correo" name="correo" placeholder="correo@servidor.com" required>
            </div>

            <div class="mb-3">
              <label for="contrasena" class="form-label">* Contraseña</label>
              <input type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Contraseña" required>
            </div>

            <div class="mb-3 pull-right mt-4 modal-footer">
              <div class="d-grid gap-2 col-12">
              <button type="submit" value="contacto" class="btn btn-outline-info btn-lg">loguin</button>
              </div>   
            </div>
          
            <?= form_close(); ?>

          <!---FIN FORMULARIO-->

        </div>
        
      </div>

    </div>
    <!---Fin de contenido  de la pagina--->
 
