<!---Contenido contenido de la pagina--->
    <div class="container-fluid mt-4 card card-body ">

      <div class="row">

        <!--Contenedor de filtros-->
        <div class="col-3">
          <div class="row mt-4 mb-4 ">
            <div class="row mt-4 p-4"> 
            Los filtros se estan diseñando
            </div>  
          </div>
         
        </div>
        <!--Contenedor de filtros-->

        <!--Contenedor de productos-->
        <div class="col-9">

          <div class="row mt-4 mb-4 ">

        
            <div class="row mt-4"><!--Comienzan los card de los productos-->
              <div class="col-sm">
                <div class="card carsiz18 " >
                  <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                  <div class="card-body">
                    <h5 class="card-title">Titulo</h5>
                    <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                    <div class="row">
                      <div class="col-4"><a href="#" class="btn btn-primary">Comprar</a></div>
                      <div class="col">
                        <select class="form-select">
                          <option value="#"></option>
                          <option value="#1">1</option>
                          <option value="#2">2</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
    
              <div class="col-sm">
                <div class="card carsiz18" >
                  <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                  <div class="card-body">
                    <h5 class="card-title">Titulo</h5>
                    <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                    <div class="row">
                      <div class="col-4"><a href="#" class="btn btn-primary">Comprar</a></div>
                      <div class="col">
                        <select class="form-select">
                          <option value="#"></option>
                          <option value="#1">1</option>
                          <option value="#2">2</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
    
              <div class="col-sm ">
                <div class="card carsiz18" >
                  <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top " alt="Imagen de producto">
                  <div class="card-body">
                    <h5 class="card-title">Titulo</h5>
                    <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                    <div class="row">
                      <div class="col-4"><a href="#" class="btn btn-primary">Comprar</a></div>
                      <div class="col">
                        <select class="form-select">
                          <option value="#"></option>
                          <option value="#1">1</option>
                          <option value="#2">2</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
    
            <div class="row mt-4">
              <div class="col-sm ">
                <div class="card carsiz18">
                  <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                  <div class="card-body">
                    <h5 class="card-title">Titulo</h5>
                    <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                    <div class="row">
                      <div class="col-4"><a href="#" class="btn btn-primary">Comprar</a></div>
                      <div class="col">
                        <select class="form-select">
                          <option value="#"></option>
                          <option value="#1">1</option>
                          <option value="#2">2</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
    
              </div>
              <div class="col-sm ">
                <div class="card carsiz18" >
                  <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                  <div class="card-body">
                    <h5 class="card-title">Titulo</h5>
                    <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                    <div class="row">
                      <div class="col-4"><a href="#" class="btn btn-primary">Comprar</a></div>
                      <div class="col">
                        <select class="form-select">
                          <option value="#"></option>
                          <option value="#1">1</option>
                          <option value="#2">2</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
    
              <div class="col-sm ">
                <div class="card carsiz18" >
                  <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                  <div class="card-body">
                    <h5 class="card-title">Titulo</h5>
                    <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                    
                    <div class="row">
                      <div class="col-4"><a href="#" class="btn btn-primary">Comprar</a></div>
                      <div class="col">
                        <select class="form-select">
                          <option value="#"></option>
                          <option value="#1">1</option>
                          <option value="#2">2</option>
                        </select>
                      </div>
                    </div>
    
                  </div>
                </div>
              </div>
            </div>
            
            <div class="row mt-4">
              <div class="col-sm ">
                <div class="card carsiz18">
                  <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                  <div class="card-body">
                    <h5 class="card-title">Titulo</h5>
                    <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                    <div class="row">
                      <div class="col-4"><a href="#" class="btn btn-primary">Comprar</a></div>
                      <div class="col">
                        <select class="form-select">
                          <option value="#"></option>
                          <option value="#1">1</option>
                          <option value="#2">2</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
    
              </div>
              <div class="col-sm ">
                <div class="card carsiz18" >
                  <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                  <div class="card-body">
                    <h5 class="card-title">Titulo</h5>
                    <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                    <div class="row">
                      <div class="col-4"><a href="#" class="btn btn-primary">Comprar</a></div>
                      <div class="col">
                        <select class="form-select">
                          <option value="#"></option>
                          <option value="#1">1</option>
                          <option value="#2">2</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
    
              <div class="col-sm ">
                <div class="card carsiz18" >
                  <img src="<?php echo base_url();?>uploads/product_ejemplo1.jpeg" class="card-img-top" alt="Imagen de producto">
                  <div class="card-body">
                    <h5 class="card-title">Titulo</h5>
                    <p class="card-text">Pequeña descripción del producto que queremos vender, solo es un texto de relleno, nada formal</p>
                    
                    <div class="row">
                      <div class="col-4"><a href="#" class="btn btn-primary">Comprar</a></div>
                      <div class="col-8">
                        <select class="form-select">
                          <option value="#"></option>
                          <option value="#1">1</option>
                          <option value="#2">2</option>
                        </select>
                      </div>
                    </div>
                    
                  </div>
                </div>
              </div>
            </div>
            <!--Finalizan card de los productos-->  
          </div>
        </div>
        <!--Contenedor de productos-->
      </div>
    </div>
    <!---Fin de contenido  de la pagina--->
