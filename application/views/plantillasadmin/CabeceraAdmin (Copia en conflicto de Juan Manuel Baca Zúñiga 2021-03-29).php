<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>archivos/css/bootstrap.min.css" rel="stylesheet">

    <!-- Personalizado CSS -->
    <link href="<?php echo base_url();?>archivos/css/css_personalizado.css" rel="stylesheet">
	<link href="<?php echo base_url();?>archivos/css/simple_sidebar.css" rel="stylesheet">
	
	<!-- Personalizado JS -->
	<script src="<?php echo base_url();?>archivos/js/jquery-3.2.1.slim.min.js"></script>
	<script src="<?php echo base_url();?>archivos/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>archivos/js/personalizado.js"></script>

	

    <title>Comercio electronico</title>
  </head>
  <body>
    <header>

    </header>
	<!--FIN HEADER-->
 <div class="d-flex" id="wrapper">
				<!-- Sidebar -->
				<div class="bg-dark border-right" id="sidebar-wrapper">
					<div class="sidebar-heading">
					<a class="card navbar-brand" href="#">
						<img class="mcentrado80"  width="120" height="auto"   alt="" loading="lazy" src="<?php echo base_url();?>uploads/img/logo_bien/logo_bien1.png"  >
					</a>

					</div>
					<div class="list-group list-group-flush">
						<a href="<?php echo base_url('shopping/index'); ?>" class="list-group-item list-group-item-action bg-light">Inicio</a>
						<a href="#" class="list-group-item list-group-item-action bg-light">Productos</a>
						<a href="#" class="list-group-item list-group-item-action bg-light">Envios</a>
						<a href="#" class="list-group-item list-group-item-action bg-light">Ventas</a>
						<a href="#" class="list-group-item list-group-item-action bg-light">Almacén</a>
						<a href="<?php echo base_url('shopping/cerrarsesion'); ?>" class="list-group-item list-group-item-action bg-light">Cerrar Sesión</a>
					</div>
				</div>
				<!-- /#sidebar-wrapper -->
