<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping extends CI_Controller {

	public function index(){
		$this->load->view('plantillas/cabecera');
		$this->load->view('plantillas/inicio');
		$this->load->view('plantillas/pie');
	}

	public function productos(){
		$this->load->view('plantillas/cabecera');
		$this->load->view('plantillas/productos');
		$this->load->view('plantillas/pie');
	}

	public function nosotros(){
		$this->load->view('plantillas/cabecera');
		$this->load->view('plantillas/nosotros');
		$this->load->view('plantillas/pie');
	}

	public function contacto(){
		$this->load->view('plantillas/cabecera');
		$this->load->view('plantillas/contacto');
		$this->load->view('plantillas/pie');
	}

	public function loguin(){
		$this->load->library('form_validation');
		$this->load->helper('personalizados_helper');
		$this->load->view('plantillas/cabecera');
		$this->load->view('plantillas/loguin');
		$this->load->view('plantillas/pie');

		$userData = array(
			'loguin'     =>$this->input->post('loguin'),
			'correo'     =>$this->input->post('correo'),
			'contrasena' => $this->input->post('contrasena')
		);
		
		
			validarLoguin($userData);
			getFlashData();
			
	}

	public function administrator(){
		$this->load->helper('personalizados_helper');
		$this->load->model('Usuarios_model');

		$this->load->view('plantillasadmin/cabeceraadmin');
		$this->load->view('plantillasadmin/contenidoadmin');
		$this->load->view('plantillasadmin/pieadmin');

		getFlashData();
		
}
	public function cerrarsesion(){
		$this->load->library('session');

		$this->session->sess_destroy();
		$this->load->helper('personalizados_helper');
		redirect('shopping/index');
}




}